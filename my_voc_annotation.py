import xml.etree.ElementTree as ET
from os import walk
from os import getcwd
from os.path import basename

# sets = [('2007', 'train'), ('2007', 'val'), ('2007', 'test')]

sets = ['Vehicles']

classes = ["motorbike", "car", "taxi", "van", "bus", "coach"]


def convert_annotation(image_set, image_id, list_file):
    in_file = open('data/{}/Annotations/{}.xml'.format(image_set, image_id))
    tree = ET.parse(in_file)
    root = tree.getroot()

    for obj in root.iter('object'):
        difficult = 0  # obj.find('difficult').text
        cls = obj.find('name').text
        if cls not in classes or int(difficult) == 1:
            continue
        cls_id = classes.index(cls)
        xmlbox = obj.find('bndbox')
        b = (int(xmlbox.find('xmin').text), int(xmlbox.find('ymin').text), int(xmlbox.find('xmax').text),
             int(xmlbox.find('ymax').text))
        list_file.write(" " + ",".join([str(a) for a in b]) + ',' + str(cls_id))


wd = getcwd()

for image_set in sets:
    # image_ids = open('data/%s.txt' % image_set).read().strip().split()
    img_ids = []
    ann_path = '{}/data/{}/Annotations'.format(wd, image_set)
    for (dirpath, dirnames, filenames) in walk(ann_path):
        anns = [name.strip('.xml') for name in filenames]
        img_ids.extend(anns)
        break

    # list_file = open('%s.txt' % image_set, 'w')
    list_file = open('%s.txt' % 'train', 'w')
    for image_id in img_ids:
        list_file.write('{}/data/{}/Images/{}.jpg'.format(wd, image_set, image_id))
        convert_annotation(image_set, image_id, list_file)
        list_file.write('\n')
    list_file.close()
